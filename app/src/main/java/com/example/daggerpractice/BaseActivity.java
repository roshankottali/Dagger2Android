package com.example.daggerpractice;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.example.daggerpractice.model.User;
import com.example.daggerpractice.ui.auth.AuthActivity;
import com.example.daggerpractice.ui.auth.AuthResource;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class BaseActivity extends DaggerAppCompatActivity {

    private static final String TAG = "BaseActivity";

    @Inject
    public SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subscribeObserver();
    }

    private void subscribeObserver() {
        sessionManager.getAuthUser().observe(this, new Observer<AuthResource<User>>() {
            @Override
            public void onChanged(AuthResource<User> userAuthResource) {
                Log.d(TAG, "INVOKED ROSHAN 2");
                if (userAuthResource != null){
                    switch (userAuthResource.status) {
                        case LOADING:
                            Log.d(TAG, "onChanged: LOADING");
                            break;
                        case AUTHENTICATED:
                            Log.d(TAG, "onChanged: AUTHENTICATED");
                            break;
                        case ERROR:
                            Log.d(TAG, "onChanged: ERROR");
                            break;
                        case NOT_AUTHENTICATED:
                            Log.d(TAG, "onChanged: NOT_AUTHENTICATED");
                            navLoginScreen();
                            break;
                    }
                }
            }
        });
    }

    private void navLoginScreen() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
        finish();
    }
}
