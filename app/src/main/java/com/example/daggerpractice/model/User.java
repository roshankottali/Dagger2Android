package com.example.daggerpractice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    @Expose()
    int mId;
    @SerializedName("username")
    @Expose()
    String mUsername;
    @SerializedName("email")
    @Expose()
    String mEmail;

    @SerializedName("website")
    @Expose()
    String mWebsite;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmWebsite() {
        return mWebsite;
    }

    public void setmWebsite(String mWebsite) {
        this.mWebsite = mWebsite;
    }

    public User(int mId, String mUsername, String mEmail, String mWebsite) {
        this.mId = mId;
        this.mUsername = mUsername;
        this.mEmail = mEmail;
        this.mWebsite = mWebsite;
    }
    public User() {
    }
}
