package com.example.daggerpractice.ui.main.posts;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.daggerpractice.SessionManager;
import com.example.daggerpractice.model.User;
import com.example.daggerpractice.ui.auth.AuthResource;

import javax.inject.Inject;

public class PostUserIdViewModel extends ViewModel {

    private static final String TAG = "PostUserIdViewModel";
    private SessionManager mSsessionManager;

    @Inject
    public PostUserIdViewModel(SessionManager sessionManager) {
        mSsessionManager = sessionManager;
        Log.d(TAG, "PostUserIdViewModel: running...");
    }

    public LiveData<AuthResource<User>> observeSateUserData() {
        return mSsessionManager.getAuthUser();
    }
}
