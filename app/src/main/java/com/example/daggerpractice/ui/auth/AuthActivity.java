package com.example.daggerpractice.ui.auth;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.RequestManager;
import com.example.daggerpractice.R;
import com.example.daggerpractice.model.User;
import com.example.daggerpractice.ui.main.MainActivity;
import com.example.daggerpractice.viewmodels.ViewModelProviderFactory;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.android.support.DaggerAppCompatActivity;

public class AuthActivity extends DaggerAppCompatActivity implements View.OnClickListener {

    private static final String TAG = "AuthActivity";

    private AuthViewModel viewModel;

    @Inject
    ViewModelProviderFactory providerFactory;

    @Inject
    Drawable logo;

    @Inject
    RequestManager requestManager;

    EditText userId;
    ProgressBar progressBar;

    @Inject
    @Named("AppUser")
    User user1;

    @Inject
    @Named("AuthUser")
    User user2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        viewModel = ViewModelProviders.of(this, providerFactory).get(AuthViewModel.class);

        userId = findViewById(R.id.user_id_input);
        progressBar = findViewById(R.id.progress_bar);
        findViewById(R.id.login_button).setOnClickListener(this);

        Log.d("MESSAGE"," " + logo);
        Log.d("USER obj"," " + user1);
        Log.d("USER obj"," " + user2);
        setLogo();

        subscribeObserver();
    }

    private void subscribeObserver() {
        viewModel.obServerState().observe(this, new Observer<AuthResource<User>>() {
            @Override
            public void onChanged(AuthResource<User> userAuthResource) {
                Log.d(TAG, "INVOKED ROSHAN 1");
                if (userAuthResource != null){
                    switch (userAuthResource.status) {
                        case LOADING:
                            Log.d(TAG, "onChanged: LOADING");
                            Toast.makeText(getApplicationContext(),"LOADING",Toast.LENGTH_LONG).show();
                            showProgressBar(true);
                            break;
                        case AUTHENTICATED:
                            Log.d(TAG, "onChanged: AUTHENTICATED");
                            Toast.makeText(getApplicationContext(),"AUTHENTICATED",Toast.LENGTH_LONG).show();
                            showProgressBar(false);
                            onLoginSuccess();
                            break;
                        case ERROR:
                            Log.d(TAG, "onChanged: ERROR");
                            Toast.makeText(getApplicationContext(),"ERROR Message: "+userAuthResource.message,Toast.LENGTH_LONG).show();
                            showProgressBar(false);
                            break;
                        case NOT_AUTHENTICATED:
                            Log.d(TAG, "onChanged: NOT_AUTHENTICATED");
                            Toast.makeText(getApplicationContext(),"NOT_AUTHENTICATED",Toast.LENGTH_LONG).show();
                            showProgressBar(false);
                            break;
                    }
                }
            }
        });
    }

    private void showProgressBar(boolean state) {
        if(state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void onLoginSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void setLogo() {
        requestManager.load(logo).into((ImageView)findViewById(R.id.login_logo));
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.login_button:
                if (!TextUtils.isEmpty(userId.getText().toString())) {
                    Toast.makeText(this, "Clicked", Toast.LENGTH_LONG).show();
                    viewModel.authenticateWithUserID(Integer.valueOf(userId.getText().toString()));
                } else {
                    Toast.makeText(this, "NUll Field Clicked", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
