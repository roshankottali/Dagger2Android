package com.example.daggerpractice.ui.main.profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.daggerpractice.R;
import com.example.daggerpractice.model.User;
import com.example.daggerpractice.ui.auth.AuthResource;
import com.example.daggerpractice.viewmodels.ViewModelProviderFactory;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class ProfileFragment extends DaggerFragment {
    private static final String TAG = "ProfileFragment";

    private ProfileViewModel viewModel;
    private TextView mEmail,mUsername,mWebsite;

    @Inject
    ViewModelProviderFactory providerFactory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toast.makeText(getActivity(),"ProfileFragment", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onCreateView: ProfileFragment");
        View view =  inflater.inflate(R.layout.fragment_profile,container,false);
        mEmail = view.findViewById(R.id.email);
        mUsername = view.findViewById(R.id.username);
        mWebsite = view.findViewById(R.id.website);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: ProfileFragment was created.....");
        viewModel = ViewModelProviders.of(this,providerFactory).get(ProfileViewModel.class);
        subsribeObservers();
    }

    private void subsribeObservers() {
        viewModel.getAuthenticateUser().removeObservers(getViewLifecycleOwner());
        viewModel.getAuthenticateUser().observe(getViewLifecycleOwner(), new Observer<AuthResource<User>>() {
            @Override
            public void onChanged(AuthResource<User> userAuthResource) {
                Log.d(TAG, "INVOKED ROSHAN 3");
                if (userAuthResource != null) {
                    switch (userAuthResource.status) {
                        case ERROR:
                            setErrorDetails(userAuthResource.message);
                            break;
                        case AUTHENTICATED:
                            if (userAuthResource.data != null) {
                                setUserDetails(userAuthResource.data);
                            }
                            break;
                    }
                }
            }
        });
    }

    private void setUserDetails(User data) {
        mEmail.setText(data.getmEmail());
        mUsername.setText(data.getmUsername());
        mWebsite.setText(data.getmWebsite());
    }

    private void setErrorDetails(String message) {
        mEmail.setText(message);
        mUsername.setText("");
        mWebsite.setText("");
    }
}
