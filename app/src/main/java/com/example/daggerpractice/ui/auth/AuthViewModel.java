package com.example.daggerpractice.ui.auth;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.ViewModel;

import com.example.daggerpractice.SessionManager;
import com.example.daggerpractice.model.User;
import com.example.daggerpractice.networks.auth.AuthApi;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class AuthViewModel extends ViewModel {
    private static final String TAG = "AuthViewModel";
    private final AuthApi mAuthApi;

    private SessionManager mSessionManager;

    @Inject
    public AuthViewModel(AuthApi authApi, SessionManager sessionManager) {
        mAuthApi = authApi;
        mSessionManager = sessionManager;
        Log.d(TAG, "AuthViewModel: viewmodel is working...");

        if (mAuthApi == null) {
            Log.d(TAG, "AuthViewModel: mAuthApi is NULL");
        } else {
            Log.d(TAG, "AuthViewModel: mAuthApi is not NULL");
        }
    }

    public void authenticateWithUserID(int userId) {
        mSessionManager.authenticateWithId(queryUserId(userId));
    }

    private LiveData<AuthResource<User>> queryUserId(int userId) {
        return LiveDataReactiveStreams.fromPublisher(
                mAuthApi.getUser(userId)
                        .onErrorReturn(new Function<Throwable, User>() {
                            @Override
                            public User apply(Throwable throwable) throws Exception {
                                User errorUser = new User();
                                errorUser.setmId(-1);
                                return errorUser;
                            }
                        })
                        .map(new Function<User, AuthResource<User>>() {
                            @Override
                            public AuthResource<User> apply(User user) throws Exception {
                                if(user.getmId() == -1) {
                                    return AuthResource.error("Could not Authenticate",(User)null);
                                }
                                return AuthResource.authenticated(user);
                            }
                        })
                        .subscribeOn(Schedulers.io()));
    }

    public LiveData<AuthResource<User>> obServerState() {
            return mSessionManager.getAuthUser();
    }
}
