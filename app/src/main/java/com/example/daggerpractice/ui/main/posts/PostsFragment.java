package com.example.daggerpractice.ui.main.posts;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.daggerpractice.R;
import com.example.daggerpractice.model.Post;
import com.example.daggerpractice.model.User;
import com.example.daggerpractice.ui.auth.AuthResource;
import com.example.daggerpractice.ui.main.Resource;
import com.example.daggerpractice.utils.VerticalSpacingItemDecoration;
import com.example.daggerpractice.viewmodels.ViewModelProviderFactory;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class PostsFragment extends DaggerFragment {
    private static final String TAG = "PostsFragment";
    private PostsViewModel viewModel;
    private PostUserIdViewModel viewModelUserDetails;
    private RecyclerView recyclerView;

    @Inject
    ViewModelProviderFactory providerFactory;

    @Inject
    PostsRecyclerAdapter recyclerAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_posts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recyclerView = view.findViewById(R.id.recycler_view);
        viewModel = ViewModelProviders.of(this, providerFactory).get(PostsViewModel.class);
        viewModelUserDetails = ViewModelProviders.of(this, providerFactory).get(PostUserIdViewModel.class);
        initRecyclerView();
        subScribeUserData();
        subscribeObserver();
    }

    private void subscribeObserver() {
        viewModel.observePost().removeObservers(getViewLifecycleOwner());
        viewModel.observePost().observe(getViewLifecycleOwner(), new Observer<Resource<List<Post>>>() {
            @Override
            public void onChanged(Resource<List<Post>> listResource) {
                if (listResource.data != null) {
                    Log.d(TAG, "onChanged: " + listResource.data.size());
                    switch (listResource.status) {
                        case ERROR:
                            Log.d(TAG, "onChanged: Error ..." + listResource.message);
                            break;
                        case LOADING:
                            Log.d(TAG, "onChanged: Loading...");
                            break;
                        case SUCCESS:
                            Log.d(TAG, "onChanged: SUCCESS...");
                            recyclerAdapter.setPosts(listResource.data);
                            break;
                    }
                }
            }
        });
    }

    private void subScribeUserData() {
        viewModelUserDetails.observeSateUserData().removeObservers(getViewLifecycleOwner());
        viewModelUserDetails.observeSateUserData().observe(getViewLifecycleOwner(), new Observer<AuthResource<User>>() {
            @Override
            public void onChanged(AuthResource<User> userAuthResource) {
                if (userAuthResource.data != null) {
                    Log.d(TAG, "onChanged: userAuthResource " + userAuthResource.data.getmId());
                    switch (userAuthResource.status) {
                        case LOADING:
                            Log.d(TAG, "onChanged: LOADING...");
                            break;
                        case ERROR:
                            Log.d(TAG, "onChanged: ERROR..." + userAuthResource.message);
                            break;
                        case AUTHENTICATED:
                            Log.d(TAG, "onChanged: AUTHENTICATED...");
                            viewModel.getPostByUserId(userAuthResource.data.getmId());
                            break;
                        case NOT_AUTHENTICATED:
                            Log.d(TAG, "onChanged: NOT_AUTHENTICATED...");
                            break;
                    }
                }
            }
        });
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        VerticalSpacingItemDecoration spacingItemDecoration = new VerticalSpacingItemDecoration(15);
        recyclerView.addItemDecoration(spacingItemDecoration);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
