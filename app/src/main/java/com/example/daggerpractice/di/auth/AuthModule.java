package com.example.daggerpractice.di.auth;

import com.example.daggerpractice.model.User;
import com.example.daggerpractice.networks.auth.AuthApi;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AuthModule {

    @AuthScope
    @Provides
    static AuthApi provideAuthApi(Retrofit retrofit) {
        return retrofit.create(AuthApi.class);
    }

    @AuthScope
    @Provides
    @Named("AuthUser")
    static User providesUser() {
        return new User();
    }
}
